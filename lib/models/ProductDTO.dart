
class ProductDTO {
  int productID,
      storeID,
      status,
      userID;
  String name,
      description,
      imageURL,
      base64Image;
  double price;

  ProductDTO({
    this.productID,
    this.storeID,
    this.name,
    this.description,
    this.price,
    this.imageURL,
    this.base64Image,
    this.status,
    this.userID,
  });

  factory ProductDTO.fromJson(Map<String, dynamic> json) {
    return ProductDTO(
      productID: json['productid'],
      storeID: json['storeid'],
      name: json['name'],
      description: json['description'],
      price: json['price'],
      imageURL: json['imageurl'],
      base64Image: json['base64image'],
      status: json['status'],
      userID: json['userid'],
    );
  }
}