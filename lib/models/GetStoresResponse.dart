
import 'StoreDTO.dart';

class GetStoresResponse {
  String message;
  bool status;
  List<StoreDTO> lsStore;

  GetStoresResponse({this.lsStore});

  GetStoresResponse.fromJson(Map<String, dynamic> json) {
    var dataJson = json['data'] as List;
    List<StoreDTO> stores = dataJson != null
        ? dataJson.map((i) => StoreDTO.fromJson(i)).toList()
        : null;

    message = json['message'];
    status = json['status'];
    lsStore = stores;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    data['data'] = this.lsStore;
    return data;
  }
}