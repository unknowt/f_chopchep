class GetStoresRequest {
  int userID;
  String name;

  GetStoresRequest({this.userID, this.name});

  factory GetStoresRequest.fromJson(Map<String, dynamic> json) {
    return GetStoresRequest(
      userID: json['userid'],
      name: json['name'],
    );
  }
}

class BaseResponse<T> {
  bool status;
  String message;
  T data;

  BaseResponse({this.status, this.message, this.data});

  factory BaseResponse.fromJson(Map<String, dynamic> json) {
    return BaseResponse(
        status: json['status'], message: json['message'], data: json['data']);
  }
}
