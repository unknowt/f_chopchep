
class StoreDTO {
  int id;
  String userid;
  String name;
  String description;
  String address;
  String contact;
  int priority;
  String imageurl;
  int status;

  StoreDTO(
      {this.id,
      this.userid,
      this.name,
      this.description,
      this.address,
      this.contact,
      this.priority,
      this.imageurl,
      this.status});

  StoreDTO.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userid = json['userid'];
    name = json['name'];
    description = json['description'];
    address = json['address'];
    contact = json['contact'];
    priority = json['priority'];
    imageurl = json['imageurl'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userid'] = this.userid;
    data['name'] = this.name;
    data['description'] = this.description;
    data['address'] = this.address;
    data['contact'] = this.contact;
    data['priority'] = this.priority;
    data['imageurl'] = this.imageurl;
    data['status'] = this.status;
    return data;
  }
}
