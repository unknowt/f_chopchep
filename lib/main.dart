import 'package:chopchep/views/home/Home.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      title: 'Chop Chep',
      home: Home(),
    ));
